## Hello! I'm Jay 👋
_Jay is my nickname, my first name is actually Jaedwin (pronounced: jade-win)_

#### About me

- I studied computer science
- I'm a certified indoor (top rope and lead) climbing instructor for the Association of Canadian Mountain Guides (ACMG)

#### My day to day

- My workday is flexible, I try to scope my workday hours from 7:00am - 6:00pm MST
- I usually try and do any MR reviews (currently a reviewer for both `frontend` and `backend`) in the morning around 10:00am - 12:00pm MST
- I'm usually out of office around 8:00am - 9:00am for school drop off and from 4:00pm - 5:00pm for school pick up
